# Compact circuits
## Objectives
This mod allows to miniaturize a set of combinators into a single combinator.

As in factorissimo, it adds a dedicated surface to edit the combinators but can run without it.

## Main features
 * up to 16 input/output point between the internal combinators and the outer objects
 * possibility to pack the internal combinators into hidden ones and to free the editor surface
 * display of the internal lamps in the external combinator with differents size of lamp depending on the count of lamps
 * mini version 1x1 of the processor with 4 input/output points
 * infinite nesting of processor
 * external display of the color of the internally connected wire to input/output pole
 * can set the direction of input/output point for external display
 * fully support of blueprint , copy/paste
 * copy/paste betwen external input/output points to easily configure the interface (exchange the internal io point and also the connected wires)

## To use it:
 * search the "Compact circuits" technology, it add a new item: the miniaturization the compact "Processor"
 * craft a processor and put it the map
 * look at the input/output point around the processor, they are numbered from 1 to 16
 * click in the center of the processor and enter the editor mode
 * add internal input/output pole (click on the left button and then put it on the map). It is connected to the external input/output points.
 * maybe, select the internal input/output point to change the index
 * you can also addd a name to a point in order to easily find the matching external point outside the editor
 * build your circuit network in the editor: you can add constant combinator, arithmetic combinator, decider combinator, power pole, input/output pole, and lamp,
 * text plates (https://mods.factorio.com/mod/textplates) and industrial display plates (https://mods.factorio.com/mod/IndustrialDisplayPlates)
 * exit from the editor
 * connect the external input/output point to the rest of of the base
 * you can see the values of the network in the editor
 * once the circuits works as expected, checked the button "Enter the packed mode".
   When you exit the editor, the associated surface is destroyed to save space.
   And you cannot see the real time values in the editor anymore but the network is working with small hidden combinators in the main surface.
   In this mode, if you add lamps in the editor, you will see them outside the editor on the processor entity.
   You can switch back to the non packed mode at any time.
   The lamps are not displayed in non-packed mode

## Settings
* Allow to change the color of the displayed elements

## Models : allow to update a set of processor in one click
 * create a new model name
 * set the name to a set of processor
 * modify one processor
 * click "apply" to apply the modifications to all processor of the same model

## Integration of other combinators
The mod contains an interface to integrate other combinators:

1. Define a "packed" version of the combinator (and any other entities used) with no display and with the properties:
   ```lua
   packed_entity.flags = { 'placeable-off-grid' , "hidden", "hide-alt-info", "not-on-map", "not-upgradable", "not-deconstructable", "not-blueprintable" }
   packed_entity.collision_mask = {}
   packed_entity.collision_box = nil
   packed_entity.minable = nil
   packed_entity.selectable_in_game = false
   ```
2. Define a remote interface in the external mod with methods:
   + `get_info(entity)`: Get private information on an entity in a structure. Properties `name`, `index`, `position`, and `direction` are reserved
   + `create_packed_entity(info, surface, position, force)`: Create a packed entity with `info` (return from previous call), `surface`, `position`, `force`
   + `create_entity(info, surface, force)`: Create a normal combinator with `info`, `surface`, `force`, `info.position`, `info.direction`
3. Call the Compakt Circuit remote interface:
   ```lua
   remote.call("compaktcircuit", "add_combinator", {
       name = '<name of the combinator>',
       packed_names = { '<list of used packed combinators>' },
       interface_name = '<interface name of the external mod interface>'
   })
   ```


See [an example of a very simple combinator](https://drive.google.com/file/d/1OG-XiAhYLdwCYssuz9pSpdL2-ovGrzbl/view?usp=sharing) for details.
